all:
	gcc -g -o taintctrl libs/sqlite3.c taintctrl.c -lpthread -ldl
	if [ -f "policy.db" ] ; then rm policy.db ; fi
	./taintctrl
	sqlite3 policy.db .dump
