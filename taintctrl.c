// Out of AOSP testing
//#define SWIRLS_TESTING
#ifdef SWIRLS_TESTING
#include "libs/sqlite3.h"
#else
#include "sqlite3.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <strings.h>
#include <signal.h>


#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <assert.h>
#include <dirent.h>
// copy file
#include <sys/stat.h>

#ifdef SWIRLS_TESTING
#define AMNIDROID_POLICY_FILE	"policy.txt"
#define AMNIDROID_POLICY_DB	"policy.db"
//#define sqlite3	sqlite
#else
#define AMNIDROID_POLICY_FILE	"/data/policy.txt"
//#define AMNIDROID_POLICY_DB	"/data/data/edu.miami.een.swirls/databases/superdb"
#define AMNIDROID_POLICY_DB	"/data/local/tmp/superdb"
#endif
#define AMNIDROID_DEVICE	"/dev/taintctrl"

/* IOCTL */
#define AMNIDROID_IOC_MAGIC	'G'
#define AMNIDROID_SYSTEM        _IO(AMNIDROID_IOC_MAGIC, 0)
#define AMNIDROID_READ          _IOR(AMNIDROID_IOC_MAGIC, 1, char *)
#define AMNIDROID_WRITE         _IOW(AMNIDROID_IOC_MAGIC, 2, char *)
#define AMNIDROID_GETTAINT      _IOR(AMNIDROID_IOC_MAGIC, 3, int *)
#define AMNIDROID_SETTAINT      _IOW(AMNIDROID_IOC_MAGIC, 4, int *)
#define AMNIDROID_CAPSULE       _IOW(AMNIDROID_IOC_MAGIC, 5, struct capsule_t *)
#define AMNIDROID_CONTEXT       _IOW(AMNIDROID_IOC_MAGIC, 6, struct context_t *)
#define AMNIDROID_POLICY        _IOW(AMNIDROID_IOC_MAGIC, 7, struct policy_t *)
#define AMNIDROID_EVAL_POLICY	_IOWR(AMNIDROID_IOC_MAGIC, 8, int *)
#define AMNIDROID_SET_NEXT_TAG	_IOW(AMNIDROID_IOC_MAGIC, 9, struct application_tag *)
#define AMNIDROID_GET_NEXT_TAG	_IOWR(AMNIDROID_IOC_MAGIC, 10, struct application_tag *)
#define AMNIDROID_GET_SOCKET_TAG	_IOWR(AMNIDROID_IOC_MAGIC, 11, struct socket_tag_u *)
#define AMNIDROID_SET_SOCKET_TAG	_IOW(AMNIDROID_IOC_MAGIC, 12, struct socket_tag_u *)
#define AMNIDROID_WHITELIST_PID	_IOW(AMNIDROID_IOC_MAGIC, 13, int *)

const char * whitelist[] = {
	"system_server",
	"servicemanager",
	"vold",
	"netd",
	"debuggerd",
	"surfaceflinger",
	"zygote",
	"drmserver",
	"mediaserver",
	"dbus-daemon",
	"installd",
	"keystore",
	"glgps",
	"adbd",
	"bluetoothd",
	"com.android.inputmethod.latin",
	"com.android.providers.calendar",
	"com.android.nfc",

};

/* Taintctrl structs */
enum TAG_RESPONSE {
	TAG_NONEXISTANT,
	TAG_BLOCK,
	TAG_ALLOW,
	TAG_ALLOW_LOG,
	TAG_UPDATE
};

enum OOC_ACTION {
	OOC_BLOCK, // Block execution or transfer of information
	OOC_ALLOW, // Allow execution or transfer of information
	OOC_ALLOW_LOG, // Allow + log everything that happen
	OOC_SUPPRIM, // Supprim data while leaving the context
	OOC_ENCRYPT
};

struct capsule_t {
	sqlite3_int64 idcapsule;
	char *name;
	int tag;
	int version;
};

struct policy_t {
	sqlite3_int64 from;
	sqlite3_int64 to;
	enum TAG_RESPONSE action;
};

struct context_t {
	sqlite3_int64 idcontext;
	char *type;
	long int time_start;
	long int time_stop;
	long int time_repeat;
	float geo_latitude;
	float geo_longitude;
	float geo_altitude;
	float geo_radius;
	enum OOC_ACTION outofcontextaction;
};

struct file_t {
	sqlite3_int64 idfile;
	char *path;
	sqlite3_int64 idcapsule;
	sqlite3_int64 idcontext;
};

struct application_t {
	sqlite3_int64 idfile;
	char *name;
	sqlite3_int64 idcapsule;
	sqlite3_int64 idcontext;
};

struct account_t {
	sqlite3_int64 idaccount;
	char *name;
	sqlite3_int64 idcapsule;
	sqlite3_int64 idcontext;
};

struct connection_t {
	sqlite3_int64 idconnection;
	char *domainname;
	sqlite3_int64 idcapsule;
	sqlite3_int64 idcontext;
};

struct socket_tag_u {
	int tag;
	char *name;
};

/* some global variables */
sqlite3 *db; /* SQLite database */
int fd = -1; /* char device file descriptor */
int quit = 0;

char *ltrim(char *s)
{
	if (s && *s)
		while(isspace(*s)) s++;
	return s;
}

char *rtrim(char *s)
{
	if (s && *s){
		char* back = s + strlen(s);
		while(isspace(*--back));
		*(back+1) = '\0';
	}
	return s;
}

char *trim(char *s)
{
	return rtrim(ltrim(s));
}

char* basename(char *name)
{
	char *cp = strrchr(name, '/');
	if (cp)
		return cp + 1;
	return name;
}

int find_pid(const char* name)
{
	DIR *dir;
	struct dirent* ent;
	char* endptr, *bbuf;
	char buf[512];
	int lpid;

	if (!(dir = opendir("/proc"))) {
		perror("can't open /proc");
		return -1;
	}

	while((ent = readdir(dir)) != NULL) {
		// * if endptr is not a null character, the directory is not
		// * entirely numeric, so ignore it
		lpid = strtol(ent->d_name, &endptr, 10);
		if (*endptr != '\0') {
			continue;
		}

		// try to open the cmdline file
		snprintf(buf, sizeof(buf), "/proc/%d/cmdline", lpid);
		FILE* fp = fopen(buf, "r");

		if (fp) {
			if (fgets(buf, sizeof(buf), fp) != NULL) {
				// check the first token in the file, the program name
				// char* first = strtok(buf, " ");
				// Go to the last / to get the basename
				bbuf = basename(buf);
				//printf("Examination of process %s\n", buf);
				if (!strncmp(bbuf, name, strlen(name))) {
					fclose(fp);
					closedir(dir);
					return lpid;
				}
			}
			fclose(fp);
		}

	}

	closedir(dir);
	return -1;
}

int update_pid(int fd, int pid){
	if(ioctl(fd, AMNIDROID_WHITELIST_PID, &pid) < 0)
		perror("System ready ioctl");

	return 0;
}


int whitelist_system_process(int fd)
{
	int n;
	int pid;

	for (n = 0; n < sizeof(whitelist) / sizeof(char *); n++){
		printf("Removing any taint for %s\n", whitelist[n]);
		pid = find_pid(whitelist[n]);
		if (pid != -1)
			update_pid(fd, pid);
	}

	return 0;
}

enum TAG_RESPONSE string_to_action(char *s)
{
	enum TAG_RESPONSE tag = TAG_NONEXISTANT;

	if (!strcmp(s, "TAG_NONEXISTANT"))
		tag = TAG_NONEXISTANT;
	if (!strcmp(s, "TAG_BLOCK"))
		tag = TAG_BLOCK;
	if (!strcmp(s, "TAG_ALLOW"))
		tag = TAG_ALLOW;
	if (!strcmp(s, "TAG_ALLOW_LOG,"))
		tag = TAG_ALLOW_LOG;
	if (!strcmp(s, "TAG_UPDATE"))
		tag = TAG_UPDATE;

	return tag;
}

int check_taint_existance(sqlite3 *db, int tag)
{
	//if (db == NULL) return 0;
	int ret;
	char req[] = "SELECT COUNT(`tag`) from `capsule` WHERE `tag` = ?";
	struct sqlite3_stmt *ppstmt;

	if ((ret = sqlite3_prepare_v2(db, req, -1, &ppstmt, NULL)) != SQLITE_OK)
	{
		fprintf(stderr, "db error: ret: %i %s",ret, sqlite3_errmsg(db));
		printf("dsfqdqds\n");
		sqlite3_close(db);
		return -1;
	}
	if (ppstmt){
		//printf("Preparing statement...\n");
		ret = sqlite3_bind_int(ppstmt, 1, tag);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
	}
	ret = sqlite3_step(ppstmt);

	ret = sqlite3_column_int(ppstmt, 0);
	sqlite3_finalize(ppstmt);

	//printf("Nb of columns: %i\n", ret);

	return ret;
}

int generate_next_taint(sqlite3 *db)
{
	int tag = 0x00010000;
	while (check_taint_existance(db, tag)){
		tag = tag << 1;
		//printf("next taint to test %i\n", tag);
	}

	printf("next taint to use %i\n", tag);
	return tag;
}

sqlite3_int64 update_capsule(sqlite3 *db, struct capsule_t *cap)
{
	int ret;
	const char *sel = "SELECT `idcapsule` from `capsule` WHERE `name` = ?";
	const char *insreq = "INSERT INTO `capsule` (`name`, `tag`, `version`) VALUES (?, ?, ?)";
	struct sqlite3_stmt *ppstmt;
	double idcap = -1;

	// check capsule existance in db

	if (sqlite3_prepare_v2(db, sel, -1, &ppstmt, NULL) != SQLITE_OK)
	{
		fprintf(stderr, "db error: %s", sqlite3_errmsg(db));
		sqlite3_close(db);
		return -1;
	}
	if (ppstmt){
		//printf("Preparing statement...\n");
		ret = sqlite3_bind_text(ppstmt, 1, cap->name, strlen(cap->name), SQLITE_TRANSIENT);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
	}
	ret = sqlite3_step(ppstmt);
	if (ret == SQLITE_ROW){
		idcap = sqlite3_column_int64(ppstmt, 0);
		sqlite3_finalize(ppstmt);
		return idcap;
	}
	sqlite3_finalize(ppstmt);
	//else create one
	if (sqlite3_prepare_v2(db, insreq, -1, &ppstmt, NULL) != SQLITE_OK)
	{
		fprintf(stderr, "db error: %s", sqlite3_errmsg(db));
		sqlite3_close(db);
		return -1;
	}
	if (ppstmt){
		//printf("Preparing statement...\n");
		ret = sqlite3_bind_text(ppstmt, 1, cap->name, strlen(cap->name), SQLITE_TRANSIENT);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int(ppstmt, 2, cap->tag);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");

		ret = sqlite3_step(ppstmt);
		if (ret != SQLITE_DONE){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite step");
		}
		ret = sqlite3_finalize(ppstmt);
		if (ret != SQLITE_OK){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite fin");
		}

		idcap = sqlite3_last_insert_rowid(db);
		//printf("returned insert id: %f\n", idcap);
	}
	return idcap;
}

sqlite3_int64 update_policy(sqlite3 *db, struct policy_t *pol)
{
	int ret;
	const char *insreq = "INSERT INTO `policy` (`cfrom`, `cto`, `action`) VALUES (?, ?, ?)";
	struct sqlite3_stmt *ppstmt;
	double idpol = -1;

	if (sqlite3_prepare_v2(db, insreq, -1, &ppstmt, NULL) != SQLITE_OK)
	{
		fprintf(stderr, "db error: %s", sqlite3_errmsg(db));
		sqlite3_close(db);
		return -1;
	}
	if (ppstmt){
		//printf("Preparing statement...\n");
		ret = sqlite3_bind_int64(ppstmt, 1, pol->from);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int64(ppstmt, 2, pol->to);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int(ppstmt, 3, pol->action);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");

		ret = sqlite3_step(ppstmt);
		if (ret != SQLITE_DONE){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite step");
		}
		ret = sqlite3_finalize(ppstmt);
		if (ret != SQLITE_OK){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite fin");
		}

		idpol = sqlite3_last_insert_rowid(db);
		//printf("returned insert id: %f\n", idpol);
	}
	return idpol;
}


sqlite3_int64 update_context(sqlite3 *db, struct context_t *ctx)
{
	int ret;
	const char *insreq = "INSERT INTO `context` (`type`, `time_start`,`time_stop`, `time_repeat`, `geo_latitude`, `geo_longitude`, `geo_altitude`, `geo_radius`, `outofcontextaction`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	struct sqlite3_stmt *ppstmt;
	double idctx = -1;

	// create insert statement
	if (sqlite3_prepare_v2(db, insreq, -1, &ppstmt, NULL) != SQLITE_OK)
	{
		fprintf(stderr, "db error: %s", sqlite3_errmsg(db));
		sqlite3_close(db);
		return -1;
	}
	if (ppstmt){
		//printf("Preparing statement...\n");
		ret = sqlite3_bind_text(ppstmt, 1, ctx->type, strlen(ctx->type), SQLITE_TRANSIENT);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int(ppstmt, 2, ctx->time_start);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int(ppstmt, 3, ctx->time_stop);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int(ppstmt, 4, ctx->time_repeat);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int(ppstmt, 5, ctx->geo_latitude);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int(ppstmt, 6, ctx->geo_longitude);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int(ppstmt, 7, ctx->geo_altitude);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int(ppstmt, 8, ctx->geo_radius);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int(ppstmt, 9, ctx->outofcontextaction);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_step(ppstmt);
		if (ret != SQLITE_DONE){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite step");
		}
		ret = sqlite3_finalize(ppstmt);
		if (ret != SQLITE_OK){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite fin");
		}
		idctx = sqlite3_last_insert_rowid(db);
		//printf("returned insert id: %f\n", idcap);
	}
	return idctx;
}

int remove_file(sqlite3 *db, char *path)
{
	int ret;
	const char *delete = "DELETE FROM `file` WHERE `path` = ?";
	struct sqlite3_stmt *ppstmt;
	double idfile = -1;

	if (sqlite3_prepare_v2(db, delete, -1, &ppstmt, NULL) != SQLITE_OK)
	{
		fprintf(stderr, "db error: %s", sqlite3_errmsg(db));
		sqlite3_close(db);
		return -1;
	}

	if (ppstmt){
		//printf("Preparing statement...\n");
		ret = sqlite3_bind_text(ppstmt, 1, path, strlen(path), SQLITE_TRANSIENT);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
	}
	ret = sqlite3_step(ppstmt);
	if (ret != SQLITE_DONE)
		perror("Problem while updating");

	ret = sqlite3_changes(db);
	fprintf(stdout, " %i fields updated", ret);
	sqlite3_finalize(ppstmt);
	return ret;
}

sqlite3_int64 update_file(sqlite3 *db, struct file_t *file)
{
	int ret;
	const char *update = "UPDATE `file` set `idcapsule` = ?  WHERE `path` = ?";
	const char *insreq = "INSERT INTO `file` (`path`, `idcapsule`, `idcontext`) VALUES (?, ?, ?)";
	struct sqlite3_stmt *ppstmt;
	double idfile = -1;

	// check file existance in db by trying to update the tag

	if (sqlite3_prepare_v2(db, update, -1, &ppstmt, NULL) != SQLITE_OK)
	{
		fprintf(stderr, "db error: %s", sqlite3_errmsg(db));
		sqlite3_close(db);
		return -1;
	}
	if (ppstmt){
		//printf("Preparing statement...\n");
		ret = sqlite3_bind_int(ppstmt, 1, file->idcapsule);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_text(ppstmt, 2, file->path, strlen(file->path), SQLITE_TRANSIENT);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
	}
	ret = sqlite3_step(ppstmt);
	if (ret != SQLITE_DONE)
		perror("Problem while updating");

	ret = sqlite3_changes(db);
	if (ret){
		//idfile = sqlite3_column_int64(ppstmt, 0);
		fprintf(stdout, " %i fields updated", ret);
		sqlite3_finalize(ppstmt);
		return idfile;
	}
	sqlite3_finalize(ppstmt);

	//else create one
	if (sqlite3_prepare_v2(db, insreq, -1, &ppstmt, NULL) != SQLITE_OK)
	{
		fprintf(stderr, "db error: %s", sqlite3_errmsg(db));
		sqlite3_close(db);
		return -1;
	}
	if (ppstmt){
		//printf("Preparing statement...\n");
		ret = sqlite3_bind_text(ppstmt, 1, file->path, strlen(file->path), SQLITE_TRANSIENT);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int64(ppstmt, 2, file->idcapsule);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int64(ppstmt, 3, file->idcontext);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");

		ret = sqlite3_step(ppstmt);
		if (ret != SQLITE_DONE){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite step");
		}
		ret = sqlite3_finalize(ppstmt);
		if (ret != SQLITE_OK){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite fin");
		}

		idfile = sqlite3_last_insert_rowid(db);
		//printf("returned insert id: %f\n", idfile);
	}
	return idfile;
}

sqlite3_int64 update_application(sqlite3 *db, struct application_t *app)
{
	int ret;
	const char *insreq = "INSERT INTO `application` (`name`, `idcapsule`, `idcontext`) VALUES (?, ?, ?)";
	struct sqlite3_stmt *ppstmt;
	double idapp = -1;

	if (sqlite3_prepare_v2(db, insreq, -1, &ppstmt, NULL) != SQLITE_OK)
	{
		fprintf(stderr, "db error: %s", sqlite3_errmsg(db));
		sqlite3_close(db);
		return -1;
	}
	if (ppstmt){
		//printf("Preparing statement...\n");
		ret = sqlite3_bind_text(ppstmt, 1, app->name, strlen(app->name), SQLITE_TRANSIENT);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int64(ppstmt, 2, app->idcapsule);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int64(ppstmt, 3, app->idcontext);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");

		ret = sqlite3_step(ppstmt);
		if (ret != SQLITE_DONE){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite step");
		}
		ret = sqlite3_finalize(ppstmt);
		if (ret != SQLITE_OK){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite fin");
		}

		idapp = sqlite3_last_insert_rowid(db);
		//printf("returned insert id: %f\n", idapp);
	}
	return idapp;
}

sqlite3_int64 update_account(sqlite3 *db, struct account_t *acc)
{
	int ret;
	const char *insreq = "INSERT INTO `account` (`name`, `idcapsule`, `idcontext`) VALUES (?, ?, ?)";
	struct sqlite3_stmt *ppstmt;
	double idacc = -1;

	if (sqlite3_prepare_v2(db, insreq, -1, &ppstmt, NULL) != SQLITE_OK)
	{
		fprintf(stderr, "db error: %s", sqlite3_errmsg(db));
		sqlite3_close(db);
		return -1;
	}
	if (ppstmt){
		//printf("Preparing statement...\n");
		ret = sqlite3_bind_text(ppstmt, 1, acc->name, strlen(acc->name), SQLITE_TRANSIENT);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int64(ppstmt, 2, acc->idcapsule);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int64(ppstmt, 3, acc->idcontext);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");

		ret = sqlite3_step(ppstmt);
		if (ret != SQLITE_DONE){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite step");
		}
		ret = sqlite3_finalize(ppstmt);
		if (ret != SQLITE_OK){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite fin");
		}

		idacc = sqlite3_last_insert_rowid(db);
		//printf("returned insert id: %f\n", idacc);
	}
	return idacc;
}

sqlite3_int64 update_connection(sqlite3 *db, struct connection_t *con)
{
	int ret;
	const char *insreq = "INSERT INTO `connection` (`domainname`, `idcapsule`, `idcontext`) VALUES (?, ?, ?)";
	struct sqlite3_stmt *ppstmt;
	double idcon = -1;

	if (sqlite3_prepare_v2(db, insreq, -1, &ppstmt, NULL) != SQLITE_OK)
	{
		fprintf(stderr, "db error: %s", sqlite3_errmsg(db));
		sqlite3_close(db);
		return -1;
	}
	if (ppstmt){
		//printf("Preparing statement...\n");
		ret = sqlite3_bind_text(ppstmt, 1, con->domainname, strlen(con->domainname), SQLITE_TRANSIENT);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int64(ppstmt, 2, con->idcapsule);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
		ret = sqlite3_bind_int64(ppstmt, 3, con->idcontext);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");

		ret = sqlite3_step(ppstmt);
		if (ret != SQLITE_DONE){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite step");
		}
		ret = sqlite3_finalize(ppstmt);
		if (ret != SQLITE_OK){
			fprintf(stderr, "errno: %i\n", ret);
			perror("sqlite fin");
		}

		idcon = sqlite3_last_insert_rowid(db);
		//printf("returned insert id: %f\n", idcon);
	}
	return idcon;
}


// Keep track of the relation between id in the policy and IDs in the database
struct c_to_db_pol_id {
	int ctx;
	sqlite3_int64 db;
	struct c_to_db_pol_id * next;
};

void update_c_to_db_pol_id(struct c_to_db_pol_id *c_to_db_root, int c_pol_id, sqlite3_int64 db_pol_id)
{
	struct c_to_db_pol_id *tmp = c_to_db_root;
	printf("update c_pol_id IDCONTEXT: %d %li\n",c_pol_id, db_pol_id);
	while (tmp->next != 0){
		tmp = tmp->next;
	}
	tmp->next = malloc(sizeof(struct c_to_db_pol_id));
	tmp->next->ctx = c_pol_id;
	tmp->next->db = db_pol_id;
	tmp->next->next = 0;
}

sqlite3_int64 resolve_c_to_db_pol_id(struct c_to_db_pol_id *c_to_db_root, int c_pol_id)
{
	struct c_to_db_pol_id *tmp = c_to_db_root;
	//printf("resolve c_pol_id: %i ",c_pol_id);
	while (tmp->next != 0){
		if (tmp->next->ctx == c_pol_id){
			//printf("resolve c_pol_id: %li ",tmp->next->db);
			return tmp->next->db;
		}
		tmp = tmp->next;
	}
	return -1;
}

void free_c_to_db_pol_id(struct c_to_db_pol_id *c_to_db_root)
{
	struct c_to_db_pol_id *tmp = c_to_db_root;
	struct c_to_db_pol_id *tmp2;

	if (tmp->next != 0){
		tmp = tmp->next;

		while (tmp->next != 0){
			tmp2 = tmp->next;
			free(tmp);
			tmp = tmp2;
		}
		free(tmp);
	}
}
// END of the context to database ID list manipulation methods

struct f_to_db_pol_id {
	int file;
	sqlite3_int64 db;
	struct f_to_db_pol_id * next;
};

void update_f_to_db_pol_id(struct f_to_db_pol_id *f_to_db_root, int f_pol_id, sqlite3_int64 db_pol_id)
{
	struct f_to_db_pol_id *tmp = f_to_db_root;
	while (tmp->next != 0){
		tmp = tmp->next;
	}
	tmp->next = malloc(sizeof(struct f_to_db_pol_id));
	tmp->next->file = f_pol_id;
	tmp->next->db = db_pol_id;
	tmp->next->next = 0;
}

sqlite3_int64 resolve_f_to_db_pol_id(struct f_to_db_pol_id *f_to_db_root, int f_pol_id)
{
	struct f_to_db_pol_id *tmp = f_to_db_root;

	while (tmp->next != 0){
		if (tmp->next->file == f_pol_id){
			return tmp->next->db;
		}
		tmp = tmp->next;
	}
	return -1;
}

void free_f_to_db_pol_id(struct f_to_db_pol_id *f_to_db_root)
{
	struct f_to_db_pol_id *tmp = f_to_db_root;
	struct f_to_db_pol_id *tmp2;

	if (tmp->next != 0){
		tmp = tmp->next;

		while (tmp->next != 0){
			tmp2 = tmp->next;
			free(tmp);
			tmp = tmp2;
		}
		free(tmp);
	}
}

#ifndef SWIRLS_TESTING
ssize_t getline(char **lineptr, size_t *n, FILE *stream)
{
	char *ptr;

	ptr = fgetln(stream, n);

	if (ptr == NULL) {
		return -1;
	}

	/* Free the original ptr */
	if (*lineptr != NULL) free(*lineptr);

	/* Add one more space for '\0' */
	size_t len = n[0] + 1;

	/* Update the length */
	n[0] = len;

	/* Allocate a new buffer */
	*lineptr = malloc(len);

	/* Copy over the string */
	memcpy(*lineptr, ptr, len-1);

	/* Write the NULL character */
	(*lineptr)[len-1] = '\0';

	/* Return the length of the new buffer */
	return len;
}
#endif

int oocaction_stoi(char *s) {
if (!strcmp(s,"OOC_BLOCK"))
	return OOC_BLOCK;
else if (!strcmp(s,"OOC_ALLOW"))
	return OOC_ALLOW;
else if (!strcmp(s,"OOC_ALLOW_LOG"))
	return OOC_ALLOW_LOG;
else if (!strcmp(s,"OOC_SUPPRIM"))
	return OOC_SUPPRIM;
else if (!strcmp(s,"OOC_ENCRYPT"))
	return OOC_ENCRYPT;
else
	return -1;
}

#define SWIRLS_STRING_SIZE 200
int read_policy(sqlite3 *db)
{
	char *s_read = NULL, *s = NULL;
	char tmp[SWIRLS_STRING_SIZE];
	char from[SWIRLS_STRING_SIZE];
	char to[SWIRLS_STRING_SIZE];
	size_t len = 0;
	ssize_t read;
	int section = 0;
	int version = 0;
	struct capsule_t cap;
	struct context_t ctx;
	struct policy_t pol;
	struct file_t file;
	struct application_t app;
	struct account_t acc;
	struct connection_t con;
	sqlite3_int64 idcap = 0;
	/*
	   int f_idpol;
	   struct f_to_db_pol_id f_to_db_root;
	   f_to_db_root.next = 0;
	 */
	struct c_to_db_pol_id c_to_db_root;
	c_to_db_root.next = 0;
	double idcontext;
	int localidcontext;
	char contexttype[10];
	char oocaction[15];

	FILE * f = fopen(AMNIDROID_POLICY_FILE, "r");
	if (f == NULL){
		perror("Policy file doesn't exist!");
		return ENOENT;
	}
	/*
	 * Read stdin
	 * Allocation of space with dynamic arrays
	 */
	while ((read = getline(&s_read, &len, f)) != -1){
		s = s_read;
		s = trim(s);
		if (strlen(s) < 3 || s[0] == '\n' || s[0] == '#'){
			continue;
		}
		printf("Reading line: %s\n", s);
//		if (!strncmp(s, "#", 1) ||  !strncmp(s, "\n", 1)){
//			printf("Comment line/new line \n");
//			continue;
//		}
		if (!strncmp(s, "[name]", 6)) {
			printf("Name section \n");
			section = 1;
			continue;
		}
		if (!strncmp(s, "[policy]", 8)) {
			printf("Policy Section \n");
			section = 2;
			continue;
		}
		if (!strncmp(s, "[contexts]", 10)) {
			printf("Contexts Section \n");
			section = 3;
			continue;
		}
		if (!strncmp(s, "[files]", 7)){
			printf("Files Section \n");
			section = 4;
			continue;
		}
		if (!strncmp(s, "[applications]", 13)){
			printf("Applications Section \n");
			section = 5;
			continue;
		}
		if (!strncmp(s, "[accounts]", 10)){
			printf("Accounts Section \n");
			section = 6;
			continue;
		}
		if (!strncmp(s, "[connections]", 13)){
			printf("Connections Section \n");
			section = 7;
			continue;
		}

		switch (section){
			// Section name
			case 1:
				bzero(tmp, SWIRLS_STRING_SIZE);
				sscanf(s, "%s %i", tmp, version);
				cap.name = tmp;
				cap.version = version;
				cap.tag = generate_next_taint(db);
				idcap = update_capsule(db, &cap);
				break;
			// Section policy
			case 2:
				bzero(tmp, SWIRLS_STRING_SIZE);
				bzero(from, SWIRLS_STRING_SIZE);
				bzero(to, SWIRLS_STRING_SIZE);
				bzero(&pol, sizeof(struct policy_t));
				bzero(&cap, sizeof(struct capsule_t));
				sscanf(s, "%s %s %s", from, to, tmp);
				cap.name = from;
				cap.tag = generate_next_taint(db);
				pol.from = update_capsule(db, &cap);
				//printf("capsule : %s %i %d\n", from, cap.tag, pol.from);
				bzero(&cap, sizeof(struct capsule_t));
				cap.name = to;
				cap.tag = generate_next_taint(db);
				pol.to = update_capsule(db, &cap);
				//printf("capsule : %s %i %d\n", to, cap.tag, pol.to);
				pol.action = string_to_action(tmp);
				update_policy(db, &pol);
				break;

			// Section context
			case 3:
				localidcontext = 0;
				bzero(contexttype, 10);
				bzero(oocaction, 15);
				bzero(&ctx, sizeof(struct context_t));
				sscanf(s, "%d %s", &localidcontext, contexttype);
				if (!strcmp(contexttype, "time-frame")){
				sscanf(s, "%d %s %s %li %li %li", &localidcontext, contexttype, oocaction,
						&ctx.time_start, &ctx.time_stop, &ctx.time_repeat);
				}
				else if (!strcmp(contexttype, "geo-loc")){
				sscanf(s, "%d %s %s %f %f %f %f", &localidcontext, contexttype, oocaction,
						&ctx.geo_latitude, &ctx.geo_longitude, &ctx.geo_altitude,
						&ctx.geo_radius);
				}
				else
					assert(0);

				ctx.type = contexttype;
				ctx.outofcontextaction = oocaction_stoi(oocaction);
				idcontext = update_context(db, &ctx);
				update_c_to_db_pol_id(&c_to_db_root, localidcontext, idcontext);

				/*
				   app.idpol = resolve_f_to_db_pol_id(&f_to_db_root, f_idpol);
				   file.idpol = resolve_f_to_db_pol_id(&f_to_db_root, f_idpol);
				   update_f_to_db_pol_id(&f_to_db_root, f_idpol, pol.idpol);
				   bzero(&ctx, sizeof(struct context_t));
				   bzero(tmp, 300);
				   sscanf(s, "%d %d %d %d", &ctx->idcontext, &ctx->time, &ctx->location, &ctx->outofcontextaction);
				   */
				break;
			// Section files
			case 4:
				bzero(&file, sizeof(struct file_t));
				bzero(tmp, 300);
				file.path = tmp;
				sscanf(s, "%s %li", file.path, &file.idcontext);
				file.idcapsule = idcap;
				file.idcontext = resolve_c_to_db_pol_id(&c_to_db_root, file.idcontext);
				update_file(db, &file);
				break;
			// Section application
			case 5:
				bzero(&app, sizeof(struct application_t));
				bzero(tmp, 300);
				app.name = tmp;
				sscanf(s, "%s %li", app.name, &app.idcontext);
				app.idcapsule = idcap;
				app.idcontext = resolve_c_to_db_pol_id(&c_to_db_root, app.idcontext);
				update_application(db, &app);
				break;
			// Section accounts
			case 6:
				bzero(&acc, sizeof(struct account_t));
				bzero(tmp, 300);
				acc.name = tmp;
				sscanf(s, "%s %li", acc.name, &acc.idcontext);
				acc.idcapsule = idcap;
				acc.idcontext = resolve_c_to_db_pol_id(&c_to_db_root, acc.idcontext);
				update_account(db, &acc);
				break;
			// Section connections
			case 7:
				bzero(&con, sizeof(struct connection_t));
				bzero(tmp, 300);
				con.domainname = tmp;
				sscanf(s, "%s %li", con.domainname, &con.idcontext);
				con.idcapsule = idcap;
				con.idcontext = resolve_c_to_db_pol_id(&c_to_db_root, con.idcontext);
				update_connection(db, &con);
				break;
			default:
				continue;
				break;
		}
	}
		free_c_to_db_pol_id(&c_to_db_root);
	//	free_f_to_db_pol_id(&f_to_db_root);
	//	if (s != NULL) free(s);
	fclose(f);
	return 0;
}

int create_db(sqlite3 *db)
{
	int rc;
	char * zErrMsg = 0;

	char table_capsule[] = "CREATE TABLE IF NOT EXISTS `capsule` (\
				`idcapsule` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,\
				`name` VARCHAR(200) NOT NULL ,\
				`tag` INT NOT NULL),\
				`version` INT NOT NULL";

	char ins_any_capsule[] = "INSERT INTO `capsule` (`name`, `tag`, `version`) VALUES (\"any\", 0, 0)";

	char table_policy[] = "CREATE TABLE IF NOT EXISTS `policy` (\
			       `idpolicy` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,\
			       `cfrom` INTEGER NOT NULL ,\
			       `cto` INTEGER NOT NULL ,\
			       `action` INT NOT NULL )";

	char table_context[] = "CREATE TABLE `context` (\
		`idcontext` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\
		`type` VARCHAR(45) NULL ,\
		`time_start` INT NULL ,\
		`time_stop` INT NULL ,\
		`time_repeat` INT NULL ,\
		`geo_latitude` REAL NULL ,\
		`geo_longitude` REAL NULL ,\
		`geo_altitude` INT NULL ,\
		`geo_radius` INT NULL ,\
		`outofcontextaction` INT NULL )";

	char table_file[] = "CREATE TABLE IF NOT EXISTS `file` (\
			     `idfile` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,\
			     `path` VARCHAR(200) NULL ,\
			     `idcapsule` INT NOT NULL,\
			     `idcontext` INT NOT NULL)";

	char table_application[] = "CREATE TABLE IF NOT EXISTS `application` (\
				    `idapplication` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,\
				    `name` VARCHAR(200) NULL ,\
				    `idcapsule` INT NOT NULL,\
				    `idcontext` INT NOT NULL)";

	char table_account[] = "CREATE TABLE IF NOT EXISTS `account` (\
				`idaccount` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,\
				`name` VARCHAR(200) NULL ,\
				`idcapsule` INT NOT NULL,\
				`idcontext` INT NOT NULL)";

	char table_connection[] = "CREATE TABLE IF NOT EXISTS `connection` (\
				   `idconnection` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,\
				   `domainname` VARCHAR(200) NULL ,\
				   `idcapsule` INT NOT NULL,\
				   `idcontext` INT NOT NULL)";

	rc = sqlite3_exec(db, table_capsule, NULL, NULL, &zErrMsg);
	if (rc != SQLITE_OK){ goto error; }

	rc = sqlite3_exec(db, ins_any_capsule, NULL, NULL, &zErrMsg);
	if (rc != SQLITE_OK){ goto error; }

	rc = sqlite3_exec(db, table_policy, NULL, NULL, &zErrMsg);
	if (rc != SQLITE_OK){ goto error; }

	rc = sqlite3_exec(db, table_context, NULL, NULL, &zErrMsg);
	if (rc != SQLITE_OK){ goto error; }

	rc = sqlite3_exec(db, table_file, NULL, NULL, &zErrMsg);
	if (rc != SQLITE_OK){ goto error; }

	rc = sqlite3_exec(db, table_application, NULL, NULL, &zErrMsg);
	if (rc != SQLITE_OK){ goto error; }

	rc = sqlite3_exec(db, table_account, NULL, NULL, &zErrMsg);
	if (rc != SQLITE_OK){ goto error; }

	rc = sqlite3_exec(db, table_connection, NULL, NULL, &zErrMsg);
	if (rc != SQLITE_OK){ goto error; }

	return rc;

error:
	fprintf(stderr, "SQL error: %s\n", zErrMsg);
	sqlite3_free(zErrMsg);
	return rc;
}

void upload_policy(sqlite3 *db, int fd)
{
	char *zErr, *endptr;
	int nrows, ncols, rc, i, j;
	char ** result;
	struct capsule_t cap;
	struct policy_t pol;
	struct connection_t con;
	struct socket_tag_u socket;
	char cap_sel[] = "SELECT `idcapsule`, `name`, `tag`, `version` FROM `capsule`;";
	char pol_sel[] = "SELECT `cfrom`, `cto`, `action` FROM `policy`;";
	char sock_sel[] = "SELECT capsule.tag, connection.domainname FROM `capsule` JOIN `connection` WHERE (connection.idcapsule = capsule.idcapsule);";
	rc = sqlite3_get_table(db, cap_sel, &result, &nrows, &ncols, &zErr);
	if (rc != SQLITE_OK){
		perror("gettable");
	}

	for(i=0; i < nrows; i++) {
		bzero(&cap, sizeof(struct capsule_t));
		for(j=0; j < ncols; j++) {
			switch (j){
				case 0:
					cap.idcapsule = strtod(result[(i+1)*ncols + j], &endptr);
					break;
				case 1:
					cap.name = result[(i+1)*ncols + j];
					break;
				case 2:
					cap.tag = strtol(result[(i+1)*ncols + j], &endptr, 10);
					break;
				case 3:
					cap.version = strtol(result[(i+1)*ncols + j], &endptr, 10);
					break;

			}
			// the i+1 term skips over the first record,
			// which is the column headers
			//fprintf(stdout, "%s", result[(i+1)*ncols + j]);
		}

		if(ioctl(fd, AMNIDROID_CAPSULE, &cap) < 0)
			perror("writing capsule to char dev");
		fprintf(stdout, "%i, %s, %i, %i\n", (int)cap.idcapsule, cap.name, cap.tag, cap.version);
	}
	// Free memory
	sqlite3_free_table(result);

	rc = sqlite3_get_table(db, pol_sel, &result, &nrows, &ncols, &zErr);
	if (rc != SQLITE_OK){
		perror("gettable");
	}

	for(i=0; i < nrows; i++) {
		bzero(&pol, sizeof(struct policy_t));
		for(j=0; j < ncols; j++) {
			switch (j){
				case 0:
					pol.from = strtod(result[(i+1)*ncols + j], &endptr);
					break;
				case 1:
					pol.to = strtod(result[(i+1)*ncols + j], &endptr);
					break;
				case 2:
					pol.action = strtol(result[(i+1)*ncols + j], &endptr, 10);
					break;
			}
			// the i+1 term skips over the first record,
			// which is the column headers
			//fprintf(stdout, "%s", result[(i+1)*ncols + j]);
		}

		if(ioctl(fd, AMNIDROID_POLICY, &pol) < 0)
			perror("writing capsule to char dev");
		fprintf(stdout, "%i, %i, %i\n", (int)pol.from, (int)pol.to, pol.action);
	}
	// Free memory
	sqlite3_free_table(result);

	rc = sqlite3_get_table(db, sock_sel, &result, &nrows, &ncols, &zErr);
	if (rc != SQLITE_OK){
		perror("gettable");
	}

	for(i=0; i < nrows; i++) {
		bzero(&socket, sizeof(struct socket_tag_u));
		for(j=0; j < ncols; j++) {
			switch (j){
				case 0:
					socket.tag = strtod(result[(i+1)*ncols + j], &endptr);
					break;
				case 1:
					socket.name = result[(i+1)*ncols + j];
					break;
			}
			// the i+1 term skips over the first record,
			// which is the column headers
			//fprintf(stdout, "%s", result[(i+1)*ncols + j]);
		}

		if(ioctl(fd, AMNIDROID_SET_SOCKET_TAG, &socket) < 0)
			perror("writing capsule to char dev");
		fprintf(stdout, "%i, %s\n", socket.tag, socket.name);
	}
	// Free memory
	sqlite3_free_table(result);
}

char * filename_to_idcapsule(sqlite3 *db, char *s)
{
	int rc, id;
	char *sql;
	char *idpol = malloc(5);
	sqlite3_stmt *stmt;
	sql = "SELECT `idcapsule` FROM `file` WHERE `path` = ?";
	sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL);
	rc = sqlite3_bind_text(stmt, 1, s, strlen(s), SQLITE_TRANSIENT);
	if (rc != SQLITE_OK)
		perror("Problem with bind statement");
	//ncols = sqlite3_column_count(stmt);
	rc = sqlite3_step(stmt);

	id = (int)sqlite3_column_int64(stmt, 0);
	sqlite3_finalize(stmt);
	sprintf(idpol, "%i", id);
	return idpol;
}

void sig_handler(int signo)
{
	if (signo == SIGINT){
		printf("received SIGINT\n");
		if(ioctl(fd, AMNIDROID_SYSTEM) < 0)
			perror("System ready ioctl");
		quit = 1;
	}
}


#ifndef BUF_SIZE        /* Allow "cc -D" to override definition */
#define BUF_SIZE 1024
#endif

int copy_file(char* path)
{
	int inputFd, outputFd, openFlags;
	mode_t filePerms;
	ssize_t numRead;
	char buf[BUF_SIZE];
	char filename[BUF_SIZE];

	sprintf(filename, "%s_init", path);

	/* Open input and output files */
	inputFd = open(path, O_RDONLY);
	if (inputFd == -1)
		fprintf(stderr, "opening file %s", path);

	openFlags = O_CREAT | O_WRONLY | O_TRUNC;
	filePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP |
		S_IROTH | S_IWOTH;      /* rw-rw-rw- */
	outputFd = open(filename, openFlags, filePerms);
	if (outputFd == -1)
		fprintf(stderr,"opening file %s", filename);

	/* Transfer data until we encounter end of input or an error */

	while ((numRead = read(inputFd, buf, BUF_SIZE)) > 0)
		if (write(outputFd, buf, numRead) != numRead)
			fprintf(stderr,"couldn't write whole buffer");
	if (numRead == -1)
		fprintf(stderr,"read");

	if (close(inputFd) == -1)
		fprintf(stderr,"close input");
	if (close(outputFd) == -1)
		fprintf(stderr,"close output");

	// exit(EXIT_SUCCESS);
return 0;
}

int dd_file(char* path)
{
	char buf[BUF_SIZE];
	sprintf(buf, "dd in=%s of=%s_0", path, path);
	system(buf);
	return 0;
}

int get_socket_taint_from_name(sqlite3 *db, char *name)
{
	int ret;
	char req[] = "SELECT capsule.tag FROM `capsule` JOIN `connection` WHERE (connection.idcapsule = capsule.idcapsule AND connection.domainname = ? )";
	struct sqlite3_stmt *ppstmt;

//	ret = sqlite3_open(AMNIDROID_POLICY_DB, &db);
//	if (ret){
//		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
//		//sqlite3_close(db);
//		return(EXIT_FAILURE);
//	}
//

	if ((ret = sqlite3_prepare_v2(db, req, -1, &ppstmt, NULL)) != SQLITE_OK)
	{
		fprintf(stderr, "db error: ret: %i %s",ret, sqlite3_errmsg(db));
		printf("dsfqdqds\n");
		sqlite3_close(db);
		return -1;
	}
	if (ppstmt){
		//printf("Preparing statement...\n");
		ret = sqlite3_bind_text(ppstmt, 1, name, strlen(name), SQLITE_TRANSIENT);
		if (ret != SQLITE_OK)
			perror("Problem with bind statement");
	}
	ret = sqlite3_step(ppstmt);

	ret = sqlite3_column_int(ppstmt, 0);
	sqlite3_finalize(ppstmt);

//	sqlite3_close(db);
	return ret;
}

int main(int argc, char **argv)
{
	int ret;
	int initialize_database = 0;
	fd_set rfds;
	//fd_set wfds;
	char buf[300];

	if (signal(SIGINT, sig_handler) == SIG_ERR){
		fprintf(stderr, "Can't catch signal\n");

	}

	/* Create the file if it doesn't exist */
	FILE * f = fopen(AMNIDROID_POLICY_DB, "r");
	if (!f){
		f = fopen(AMNIDROID_POLICY_DB, "w+");
		initialize_database = 1;
	}
	fclose(f);

	ret = sqlite3_open(AMNIDROID_POLICY_DB, &db);
	if (ret){
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);
		return(EXIT_FAILURE);
	}

	if (initialize_database){
		ret = create_db(db);
		ret = read_policy(db);
	}

#ifndef SWIRLS_TESTING

	if ((fd = open(AMNIDROID_DEVICE, O_RDWR)) < 0) {
		perror("open");
		return -1;
	}

	// Make sure you don't propagate taint to system process
	whitelist_system_process(fd);
	// Send capsule informations to the kernel
	upload_policy(db, fd);

	if(ioctl(fd, AMNIDROID_SYSTEM) < 0)
		perror("System ready ioctl");

	// Watch stdin (fd 0) to see when it has input.
	FD_ZERO(&rfds);
	//FD_ZERO(&wfds);
	FD_SET(fd, &rfds);
	//FD_SET(fd, &wfds);

	for (;;)
	{
		ret = select(fd+1, &rfds, NULL, NULL, NULL);

		if (quit)
			break;

		if (ret == -1)
			perror("select()");

		if (FD_ISSET(fd, &rfds)) {
			// read from char dev
			if(ioctl(fd, AMNIDROID_READ, buf) < 0)
				perror("Reading path from char dev");

			printf("read: %s", buf);

			if (buf[0] == 'W'){
				struct file_t file;
				char path[300];
				bzero(&file, sizeof(struct file_t));
				bzero(path, 300);
				file.path = path;
				sscanf(buf + 2, "%d %s", &file.idcapsule, file.path);
				fprintf(stdout, " ...file update\n");
				file.idcontext = 0;
				update_file(db, &file);
			}

			if (buf[0] == 'R'){
				/*
				 * Add if it is a read or write opening, based on the case copy the file with the taint
				 * Cheap Namespace system...
				 * */
				char *t = filename_to_idcapsule(db, buf+4);
				//if (buf[2] == 'W' && *t == '0'){
					//copy_file(buf[4]);
				//	dd_file(buf[4]);
				//}
				if(ioctl(fd, AMNIDROID_WRITE, t) < 0)
					perror("writing taint error:");
				printf(" ...answered taint: %s\n", t);
				free(t);
			}

			if (buf[0] == 'D'){
				remove_file(db, buf +2);
				printf(" ...file removed\n");

			}
//			if (buf[0] == 'S'){
//				int tag = get_socket_taint_from_name(db, buf+2);
//				char t[20];
//				sprintf(t, "%d", tag);
//				if(ioctl(fd, AMNIDROID_WRITE, t) < 0)
//					perror("writing taint error:");
//				printf(" ...answered taint: %s\n", t);
//			}
			memset(buf, 0, 300);
		}
	}

	close(fd);

#endif // ! SWIRLS_TESTING

	sqlite3_close(db);
	return EXIT_SUCCESS;
}

