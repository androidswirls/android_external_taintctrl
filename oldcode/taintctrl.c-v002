/* inspitation:
 * http://people.ee.ethz.ch/~arkeller/linux/multi/kernel_user_space_howto-3.html
 * http://www.linuxjournal.com/article/7356
 * http://www.infradead.org/~tgr/libnl/doc/core.html
 * http://www.linuxfoundation.org/collaborate/workgroups/networking/genericnetlinkhowto
 */
/*
 * Change fprintf by ALOG()
 *
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <poll.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <signal.h>

#include <linux/genetlink.h>

/*
 * Generic macros for dealing with netlink sockets. Might be duplicated
 * elsewhere. It is recommended that commercial grade applications use
 * libnl or libnetlink and use the interfaces provided by the library
 */
#define GENLMSG_DATA(glh) ((void *)(NLMSG_DATA(glh) + GENL_HDRLEN))
#define GENLMSG_PAYLOAD(glh) (NLMSG_PAYLOAD(glh, 0) - GENL_HDRLEN)
#define NLA_DATA(na) ((void *)((char*)(na) + NLA_HDRLEN))
//#define NLA_PAYLOAD(len) (len - NLA_HDRLEN)

struct netlink_msg{
    struct nlmsghdr n;
    struct genlmsghdr g;
    char buf[256];
} netlink_msga;

int done = 0;
int seq = 0; /* seq number */
int id; /* Netlink familly ID */
int nl_sd; /*the socket*/
/*
 * Create a raw netlink socket and bind
 */
static int create_nl_socket(int protocol, int groups)
{
    socklen_t addr_len;
    int fd;
    struct sockaddr_nl local;

    fd = socket(AF_NETLINK, SOCK_RAW, protocol);
    if (fd < 0){
        perror("socket");
        return -1;
    }

    memset(&local, 0, sizeof(local));
    local.nl_family = AF_NETLINK;
    local.nl_groups = groups;
    if (bind(fd, (struct sockaddr *) &local, sizeof(local)) < 0)
        goto error;

    return fd;
error:
    close(fd);
    return -1;
}

/*
 * Send netlink message to kernel
 */
int sendto_fd(int s, const char *buf, int bufLen)
{
    struct sockaddr_nl nladdr;
    int r;

    memset(&nladdr, 0, sizeof(nladdr));
    nladdr.nl_family = AF_NETLINK;

    while ((r = sendto(s, buf, bufLen, 0, (struct sockaddr *) &nladdr,
                    sizeof(nladdr))) < bufLen) {
        if (r > 0) {
            buf += r;
            bufLen -= r;
        } else if (errno != EAGAIN)
            return -1;
    }
    return 0;
}


/*
 * Probe the controller in genetlink to find the family id
 * for the TAINTCTRL family
 */
int get_family_id(int sd)
{
    struct nlattr *na;
    int rep_len;
    struct netlink_msg family_req, ans;

    /* Get family name */
    family_req.n.nlmsg_type = GENL_ID_CTRL;
    family_req.n.nlmsg_flags = NLM_F_REQUEST;
    family_req.n.nlmsg_seq = seq++;
    family_req.n.nlmsg_pid = getpid();
    family_req.n.nlmsg_len = NLMSG_LENGTH(GENL_HDRLEN);
    family_req.g.cmd = CTRL_CMD_GETFAMILY;
    family_req.g.version = 0x1;

    na = (struct nlattr *) GENLMSG_DATA(&family_req);
    na->nla_type = CTRL_ATTR_FAMILY_NAME;
    /*------change here--------*/
    na->nla_len = strlen("TAINTCTRL") + 1 + NLA_HDRLEN;
    strcpy(NLA_DATA(na), "TAINTCTRL");

    family_req.n.nlmsg_len += NLMSG_ALIGN(na->nla_len);

    if (sendto_fd(sd, (char *) &family_req, family_req.n.nlmsg_len) < 0)
        return -1;

    rep_len = recv(sd, &ans, sizeof(ans), 0);
    if (rep_len < 0){
        perror("recv");
        return -1;
    }

    /* Validate response message */
    if (!NLMSG_OK((&ans.n), rep_len)){
        fprintf(stderr, "invalid reply message\n");
        return -1;
    }

    if (ans.n.nlmsg_type == NLMSG_ERROR) { /* error */
        fprintf(stderr, "received error\n");
        return -1;
    }

    na = (struct nlattr *) GENLMSG_DATA(&ans);
    na = (struct nlattr *) ((char *) na + NLA_ALIGN(na->nla_len));
    if (na->nla_type == CTRL_ATTR_FAMILY_ID) {
        id = *(__u16 *) NLA_DATA(na);
    }
    return id;
}

int answer_request(char * data)
{
    struct netlink_msg ans;
    struct nlattr *na;
    /* Send command needed */
    ans.n.nlmsg_len = NLMSG_LENGTH(GENL_HDRLEN);
    ans.n.nlmsg_type = id;
    ans.n.nlmsg_flags = NLM_F_REQUEST;
    ans.n.nlmsg_seq = seq++;
    ans.n.nlmsg_pid = getpid();
    ans.g.cmd = 1;// TAINTCTRL_C_FILETAINT;

    /*compose message*/
    na = (struct nlattr *) GENLMSG_DATA(&ans);
    na->nla_type = 1; //TAINTCTRL_A_MSG
    char * message =  strcat(data, ";1"); //message
    int mlength = strlen(message) + 1;
    na->nla_len = mlength+NLA_HDRLEN; //message length
    memcpy(NLA_DATA(na), message, mlength);
    ans.n.nlmsg_len += NLMSG_ALIGN(na->nla_len);

    /*send message*/
    struct sockaddr_nl nladdr;
    int r;

    memset(&nladdr, 0, sizeof(nladdr));
    nladdr.nl_family = AF_NETLINK;

    r = sendto(nl_sd, (char *)&ans, ans.n.nlmsg_len, 0,  
            (struct sockaddr *) &nladdr, sizeof(nladdr));
    return r;
}


void process_requests(int nl_sd)
{    
    struct netlink_msg req;
    struct nlattr *na;
    int req_len;

    req_len = recv(nl_sd, &req, sizeof(req), 0);
    if (req_len < 0){
        perror("error receiving reply message via Netlink \n");
        return -1;
    }

    /* Validate response message */
    if (!NLMSG_OK((&req.n), req_len)){
        perror("invalid reply message received via Netlink\n");
        return -1;
    }

    if (req.n.nlmsg_type == NLMSG_ERROR) { /* error */
        perror("error received NACK - leaving \n");
        return -1;
    }

    req_len = GENLMSG_PAYLOAD(&req.n);
  
    /*parse reply message*/
    na = (struct nlattr *) GENLMSG_DATA(&req);
    char * result = (char *)NLA_DATA(na);
    printf("kernel says: %s\n",result);
    answer_request(result);
}

int main()
{
    nl_sd = create_nl_socket(NETLINK_GENERIC,0);
    if(nl_sd < 0){
        printf("create failure\n");
        return 0;
    }
    get_family_id(nl_sd);

    for(;;)
        process_requests(nl_sd);

    close(nl_sd);
    return EXIT_SUCCESS; 
}


